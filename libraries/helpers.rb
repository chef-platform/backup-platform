# frozen_string_literal: true

#
# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# BackupPlatform module
module BackupPlatform
  def exports(name, remotes)
    remotes.map do |remote, conf|
      remote_name, dest = remote.split(':')
      next if dest.nil? && conf['remote'].nil?

      "backup-#{name}-export-#{remote_name}.service"
    end.compact.join(' ')
  end

  def service_unit_template(description, command, log_pre, log_post)
    {
      'Unit' => { 'Description' => description },
      'Service' => {
        'Type' => 'oneshot',
        'ExecStart' => "/bin/bash -c '#{command}'",
        'ExecStartPre' => "/bin/echo '#{log_pre}'",
        'ExecStartPost' => "/bin/echo '#{log_post}'"
      }
    }
  end

  def timer_unit_template(description, oncalendar)
    {
      'Unit' => { 'Description' => description },
      'Timer' => { 'OnCalendar' => oncalendar },
      'Install' => { 'WantedBy' => 'timers.target' }
    }
  end
end
