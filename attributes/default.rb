# frozen_string_literal: true

#
# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

cookbook_name = 'backup-platform'

# Default config for backups, will be merged with specific config
# See 'backups' for explanations
default[cookbook_name]['backups_default'] = {
  'local_retention' => 5,
  'timer' => '*-*-* 04:00:00',
  'backup_dir' => '/tmp/backup/%{name}',
  'backup_dir_options' => {},
  'remotes' => {}
}

# Backups definitions
default[cookbook_name]['backups'] = {
  # Backup name, used by default to define backup dir
  # 'default' => {

  # How many backups we keep locally
  # 'local_retention' => 15,

  # 'timer' => '*-*-* 04:00:00',

  # Where to find the backup produced
  # 'backup_dir' => '/tmp/backups/%{name}'
  # An hash to specify backup directory resource properties
  # 'backup_dir_options' => {
  #     'owner' => 'root',
  #     'group' => 'wheel',
  #     'mode' => '750',
  #     'recursive' => 'true'
  #   }
  # }

  # Backup command to launch
  # 'command' => '/bin/echo some > /tmp/backups/default/backup-$(date +%%F)',

  # Where to export backup
  # 'remotes' => {

  # Export and its configuration
  # Export name should follow pattern remote_name:path
  # Path should be empty if remote is intended to be crypt
  #   'local-test:/mnt/backup/test' => {

  # In the following, you can use any option of a rclone remote
  # See https://rclone.org/docs
  #     'type' => 'local',
  #   }
  # }
}

# Where to put rclone installation dir
default[cookbook_name]['prefix_root'] = '/opt'
# Where to link rclone installation dir
default[cookbook_name]['prefix_home'] = '/opt'
# Where to link rclone binaries
default[cookbook_name]['prefix_bin'] = '/opt/bin'

# Rclone version
default[cookbook_name]['version'] = '1.44'
version = node[cookbook_name]['version']

default[cookbook_name]['architecture'] = 'linux-amd64'
architecture = node[cookbook_name]['architecture']

archive = "rclone-v#{version}-#{architecture}"
default[cookbook_name]['mirror'] =
  "https://downloads.rclone.org/v#{version}/#{archive}.zip"

default[cookbook_name]['checksum'] =
  '73e166e14fbf3955e4c0dfca976821d76ba6744f14c07e1848a0580924dc211d'

# Configure retries for the package resources, default = global default (0)
# (mostly used for test purpose)
default[cookbook_name]['package_retries'] = nil
