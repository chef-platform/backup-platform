# frozen_string_literal: true

#
# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

::Chef::Recipe.send(:include, BackupPlatform)

# This lambda is used to add extract service dependencies and
# execution order built upon recipes included in run list
add_dependencies = lambda do |unit, global, exports|
  depends = []
  depends << exports if node['recipes'].include?("#{cookbook_name}::export")
  depends << global if node['recipes'].include?("#{cookbook_name}::global")
  unit['Unit']['Before'] = depends.join(' ') unless depends.empty?
  unit['Unit']['After'] = 'network.target'
  unit
end

# Generate extract service for each backup
node[cookbook_name]['backups'].each_pair do |name, config|
  conf = node[cookbook_name]['backups_default'].merge(config)
  global_service = "backup-#{name}.service"
  extract_service = "backup-#{name}-extract.service"
  export_str = exports(name, conf['remotes'].to_h)
  dir = format(conf['backup_dir'], name: name)

  # Create backup directory
  resource = directory dir
  conf['backup_dir_options'].each { |k, v| resource.send(k, v) }

  log = "#{name} extract step to #{dir}"
  extract_unit = service_unit_template(
    extract_service,
    conf['command'],
    "Starting #{log}",
    "Finished #{log}"
  )

  # Create extract systemd service unit
  systemd_unit extract_service do
    content(lazy do
      add_dependencies.call(extract_unit, global_service, export_str)
    end)
    action %i[create]
  end
end
