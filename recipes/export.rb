# frozen_string_literal: true

#
# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

::Chef::Recipe.send(:include, BackupPlatform)

# This lambda is used to add export service dependencies and
# execution order built upon recipes included in run list
add_dependencies = lambda do |unit, global, extract|
  if node['recipes'].include?("#{cookbook_name}::extract")
    unit['Unit']['Before'] = global
  end
  if node['recipes'].include?("#{cookbook_name}::extract")
    unit['Unit']['After'] = "network.target #{extract}"
  end
  unit
end

# Generate export services for each backup
# rubocop:disable  Metrics/BlockLength
node[cookbook_name]['backups'].each_pair do |name, config|
  conf = node[cookbook_name]['backups_default'].merge(config)
  global_service = "backup-#{name}.service"
  extract_service = "backup-#{name}-extract.service"
  rclone_conf = {}
  rclone_conf_file =
    "#{node[cookbook_name]['prefix_home']}/rclone/rclone-#{name}.conf"

  # Iterate through remotes to generate export command
  conf['remotes'].each_pair do |remote, remote_conf|
    remote_name, dest = remote.split(':')

    # Create directory if remote is of local type
    if dest && remote_conf['type'] == 'local'
      directory dest do
        recursive true
        mode '0755'
      end
    end

    # If remote is intended to be used to crypt backup data
    # We do not have to generate export service
    rclone_conf[remote_name] = remote_conf
    next if dest.nil? && remote_conf['remote'].nil?

    dir = format(conf['backup_dir'], name: name)
    export_command =
      "#{node[cookbook_name]['prefix_bin']}/rclone " \
      "--verbose --config #{rclone_conf_file} " \
      "copy #{dir} #{remote}"

    log = "#{name} export step to #{remote_name}"
    export_service = "backup-#{name}-export-#{remote_name}.service"
    export_unit = service_unit_template(
      export_service,
      export_command,
      "Starting #{log}",
      "Finished #{log}"
    )

    # Create export systemd service unit
    systemd_unit export_service do
      content(lazy do
        add_dependencies.call(export_unit, global_service, extract_service)
      end)
      action :create
    end
  end

  rclone_ini_conf = systemd_unit 'rclone_conf.to_ini' do
    content rclone_conf
    action :nothing
  end.to_ini

  # Create backup rclone configuration
  file rclone_conf_file do
    mode '0700'
    content rclone_ini_conf
  end
end
# rubocop:enable  Metrics/BlockLength
