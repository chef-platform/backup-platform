# frozen_string_literal: true

#
# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

::Chef::Recipe.send(:include, BackupPlatform)

# This lambda is used to add global service dependencies and
# execution order built upon recipes included in run list
add_dependencies = lambda do |unit, extract, exports|
  depends = []
  depends << extract if node['recipes'].include?("#{cookbook_name}::extract")
  depends << exports if node['recipes'].include?("#{cookbook_name}::export")
  unit['Unit']['After'] = 'network.target'
  unless depends.empty?
    unit['Unit']['After'] += " #{depends.join(' ')}"
    unit['Unit']['Requires'] = depends.join(' ')
  end
  unit
end

# Generate global service for each backup
# rubocop:disable  Metrics/BlockLength
node[cookbook_name]['backups'].each_pair do |name, config|
  conf = node[cookbook_name]['backups_default'].merge(config)
  extract_service = "backup-#{name}-extract.service"
  global_service = "backup-#{name}.service"
  export_str = exports(name, conf['remotes'])
  dir = format(conf['backup_dir'], name: name)

  command = "ls -tp #{dir}"
  # Add backup dir info
  log = "#{name} global step"

  # Activate retention if needed
  unless conf['local_retention'].nil?
    retention_command =
      " | grep -v \\'/$\\' | tail -n +#{conf['local_retention'] + 1} | " \
      "xargs -I {} rm -v -- #{dir}/{}"
    command += retention_command
    log += " (retention: #{conf['local_retention']})"
  end

  global_unit = service_unit_template(
    global_service,
    command,
    "Starting #{log}",
    "Finished #{log}"
  )

  # Create global systemd service unit
  systemd_unit global_service do
    content(lazy do
      add_dependencies.call(global_unit, extract_service, export_str)
    end)
    action %i[create]
  end

  # Create global systemd timer unit
  timer_unit = timer_unit_template("backup-#{name}.timer", conf['timer'])
  systemd_unit "backup-#{name}.timer" do
    content timer_unit
    action %i[create enable start]
  end
end
# rubocop:enable  Metrics/BlockLength
