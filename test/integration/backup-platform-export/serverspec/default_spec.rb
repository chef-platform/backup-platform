# frozen_string_literal: true

#
# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'
require 'rclone_helper'
require 'export_helper'

describe file(rclone_file) do
  it 'should be a file' do
    expect(subject).to be_file
  end

  it 'should have rclone configuration' do
    expect(subject.content).to eq(rclone_config)
  end
end

describe file("/etc/systemd/system/#{service_name}.service") do
  it 'should be a file' do
    expect(subject).to be_file
  end

  it 'should have export service unit content' do
    expect(subject.content).to eq(service_content)
  end
end

describe command("systemctl status #{service_name}") do
  it 'should be loaded' do
    expect(subject.stdout).to match(/Loaded: loaded/)
  end

  it 'should be inactive' do
    expect(subject.stdout).to match('Active: inactive \(dead\)')
  end
end

describe file("#{rclone_remote['dir']}/nivdhqfee8vuanrg555d7s4nnk") do
  it 'should be a file' do
    expect(subject).to be_file
  end
end

file = 'file-uncrypted'
describe command("#{rclone_cmd} ls #{rclone_remote['name']}:") do
  it 'should list transferred file' do
    expect(subject.stdout).to match("15 #{file}")
  end
end

describe command("#{rclone_cmd} cat #{rclone_remote['name']}:#{file}") do
  it 'should show content of crypted file' do
    expect(subject.stdout).to eq("this is a test\n")
  end
end
