# frozen_string_literal: true

#
# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF serviceKIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'
require 'retention_helper'

service_content.each do |service, content|
  describe file("/etc/systemd/system/#{service}.service") do
    it 'should be a file' do
      expect(subject).to be_file
    end

    it "should have #{service.split('-')[2]} service unit content" do
      expect(subject.content).to eq(content)
    end
  end
  describe command("systemctl status #{service}") do
    it 'should be loaded' do
      expect(subject.stdout).to match(/Loaded: loaded/)
    end

    regexp = 'Active: (inactive|activating)'
    it 'should be inactive or activating' do
      expect(subject.stdout).to match(regexp)
    end
  end
end

describe file("/etc/systemd/system/#{service_name['global']}.timer") do
  it 'should be a file' do
    expect(subject).to be_file
  end

  it 'should have global timer unit content' do
    expect(subject.content).to eq(global_timer_content)
  end
end

describe command("systemctl status #{service_name['global']}.timer") do
  it 'should be loaded' do
    expect(subject.stdout).to match(/Loaded: loaded/)
  end

  it 'should be inactive' do
    expect(subject.stdout).to match('Active: inactive \(dead\)')
  end
end

describe file(extract_dir) do
  it 'should be a directory' do
    expect(subject).to be_directory
  end

  it 'should be owned by root' do
    expect(subject).to be_owned_by('root')
  end

  it 'should belong to group root' do
    expect(subject).to be_grouped_into('root')
  end

  it 'should be mode 0755' do
    expect(subject).to be_mode(755)
  end
end

n = 3
describe command("ls #{extract_dir} | wc -l") do
  it "should count #{n} backup items" do
    expect(subject.stdout).to eq("#{n}\n")
  end
end
