# frozen_string_literal: true

name 'backup-platform-global'

run_list(
  'recipe[backup-platform]'
)

default_attributes(
  'backup-platform-test' => {
    'service' => 'backup-test-extract'
  },
  'backup-platform' => {
    'backups' => {
      'test' => {
        'backup_dir_options' => {
          'owner' => 'root',
          'group' => 'wheel',
          'mode' => '0700',
          'recursive' => true
        },
        'timer' => '*-*-* *:*:00',
        'local_retention' => 1,
        'command' => '/bin/echo test > '\
          '/tmp/backup/test/test-$(date +%%F)',
        'remotes' => {
          'local-plain:/mnt/backup/plain' => {
            'type' => 'local',
            'nounc' => true
          },
          'local-crypt:' => {
            'type' => 'local',
            'nounc' => true
          },
          'remote-crypt:' => {
            'type' => 'crypt',
            'remote' => 'local-crypt:/mnt/backup/crypt',
            'filename_encryption' => 'standard',
            'directory_name_encryption' => 'false',
            'password' => 'RtsjSxAP2klMtZvAh62yZH1dTIdmTFwivQV1-LU-VqRa',
            'password2' => 'E1aukiBnxYS_GoAY1lkitFFsddyzBbMDOWDMjSaHUiP'\
              'bnXNS0cjRUJ8'
          }
        }
      }
    }
  }
)
