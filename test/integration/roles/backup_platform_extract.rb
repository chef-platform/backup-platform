# frozen_string_literal: true

name 'backup-platform-extract'

run_list(
  'recipe[backup-platform::install]',
  'recipe[backup-platform::extract]'
)

default_attributes(
  'backup-platform-test' => {
    'service' => 'backup-test-extract'
  },
  'backup-platform' => {
    'backups' => {
      'test' => {
        'backup_dir_options' => {
          'recursive' => true
        },
        'timer' => '*-*-* *:*:00',
        'local_retention' => 1,
        'command' => '/bin/echo test > '\
          '/tmp/backup/test/test-$(date +%%F)'
      }
    }
  }
)
