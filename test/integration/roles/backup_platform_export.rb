# frozen_string_literal: true

name 'backup-platform-export'

run_list(
  'recipe[backup-platform::install]',
  'recipe[backup-platform::export]'
)

default_attributes(
  'backup-platform-test' => {
    'service' => 'backup-test-extract'
  },
  'backup-platform' => {
    'backups' => {
      'test' => {
        'remotes' => {
          'remote-local:' => {
            'type' => 'local',
            'nounc' => true
          },
          'remote-crypt:' => {
            'type' => 'crypt',
            'remote' => 'remote-local:/mnt/backup/test',
            'filename_encryption' => 'standard',
            'directory_name_encryption' => 'false',
            'password' => 'RtsjSxAP2klMtZvAh62yZH1dTIdmTFwivQV1-LU-VqRa',
            'password2' => 'E1aukiBnxYS_GoAY1lkitFFsddyzBbMDOWDMjSaHUiP'\
              'bnXNS0cjRUJ8'
          }
        }
      }
    }
  }
)
