# frozen_string_literal: true

#
# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

def extract_dir
  '/tmp/backup/test'
end

def service_name
  {
    'extract' => 'backup-test-extract',
    'global' => 'backup-test'
  }
end

def service_content
  {
    'backup-test-extract' => extract_service_content,
    'backup-test' => global_service_content
  }
end

def extract_service_content
  cmd = "/bin/echo test > #{extract_dir}/test-$(date +%%F-%%s)-$(uuidgen -t)"
  <<-CONTENT.gsub(/^ */, '')
    [Unit]
    Description = #{service_name['extract']}.service
    Before = #{service_name['global']}.service
    After = network.target

    [Service]
    Type = oneshot
    ExecStart = /bin/bash -c '#{cmd}'
    ExecStartPre = /bin/echo 'Starting test extract step to #{extract_dir}'
    ExecStartPost = /bin/echo 'Finished test extract step to #{extract_dir}'
  CONTENT
end

def global_service_content
  cmd = "ls -tp #{extract_dir} | grep -v \\\'/$\\\' | tail -n +4 " \
      "| xargs -I {} rm -v -- #{extract_dir}/{}"
  <<-CONTENT.gsub(/^ */, '')
    [Unit]
    Description = #{service_name['global']}.service
    After = network.target #{service_name['extract']}.service
    Requires = #{service_name['extract']}.service

    [Service]
    Type = oneshot
    ExecStart = /bin/bash -c '#{cmd}'
    ExecStartPre = /bin/echo 'Starting test global step (retention: 3)'
    ExecStartPost = /bin/echo 'Finished test global step (retention: 3)'
  CONTENT
end

def global_timer_content
  <<-CONTENT.gsub(/^ */, '')
      [Unit]
      Description = #{service_name['global']}.timer

      [Timer]
      OnCalendar = *-*-* *:*:00

      [Install]
      WantedBy = timers.target
  CONTENT
end
