# frozen_string_literal: true

#
# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'rclone_helper'

def service_name
  'backup-test-export-remote-crypt'
end

def service_content
  cmd = "#{rclone_cmd} copy /tmp/backup/test #{rclone_remote['name']}:"
  remote = rclone_remote['name']
  <<-CONTENT.gsub(/^ */, '')
    [Unit]
    Description = #{service_name}.service

    [Service]
    Type = oneshot
    ExecStart = /bin/bash -c '#{cmd}'
    ExecStartPre = /bin/echo 'Starting test export step to #{remote}'
    ExecStartPost = /bin/echo 'Finished test export step to #{remote}'
  CONTENT
end

def rclone_remote
  {
    'name' => 'remote-crypt',
    'dir' => '/mnt/backup/test'
  }
end

def rclone_config
  <<-RCLONE_CONF.gsub(/^ */, '')
    [remote-local]
    type = local
    nounc = true

    [#{rclone_remote['name']}]
    type = crypt
    remote = remote-local:#{rclone_remote['dir']}
    filename_encryption = standard
    directory_name_encryption = false
    password = RtsjSxAP2klMtZvAh62yZH1dTIdmTFwivQV1-LU-VqRa
    password2 = E1aukiBnxYS_GoAY1lkitFFsddyzBbMDOWDMjSaHUiPbnXNS0cjRUJ8
  RCLONE_CONF
end
