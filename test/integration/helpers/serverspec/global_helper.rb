# frozen_string_literal: true

#
# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'rclone_helper'

def extract_dir
  '/tmp/backup/test'
end

def service_name
  {
    'extract' => 'backup-test-extract',
    'export' => {
      'plain' => 'backup-test-export-local-plain',
      'crypt' => 'backup-test-export-remote-crypt'
    },
    'global' => 'backup-test'
  }
end

def service_content
  {
    'backup-test-extract' => extract_service_content,
    'backup-test-export-local-plain' => export_plain_service_content,
    'backup-test-export-remote-crypt' => export_crypt_service_content,
    'backup-test' => global_service_content
  }
end

def extract_service_content
  cmd = "/bin/echo test > #{extract_dir}/test-$(date +%%F)"
  exports = service_name['export'].map { |_, v| "#{v}.service" }.join(' ')
  <<-CONTENT.gsub(/^ */, '')
    [Unit]
    Description = #{service_name['extract']}.service
    Before = #{exports} #{service_name['global']}.service
    After = network.target

    [Service]
    Type = oneshot
    ExecStart = /bin/bash -c '#{cmd}'
    ExecStartPre = /bin/echo 'Starting test extract step to #{extract_dir}'
    ExecStartPost = /bin/echo 'Finished test extract step to #{extract_dir}'
  CONTENT
end

# rubocop:disable Metrics/AbcSize
def export_plain_service_content
  remote = rclone_remote['plain']['name']
  cmd = "#{rclone_cmd} copy #{extract_dir} " \
    "#{remote}:#{rclone_remote['plain']['dir']}"
  <<-CONTENT.gsub(/^ */, '')
    [Unit]
    Description = #{service_name['export']['plain']}.service
    Before = #{service_name['global']}.service
    After = network.target #{service_name['extract']}.service

    [Service]
    Type = oneshot
    ExecStart = /bin/bash -c '#{cmd}'
    ExecStartPre = /bin/echo 'Starting test export step to #{remote}'
    ExecStartPost = /bin/echo 'Finished test export step to #{remote}'
  CONTENT
end

def export_crypt_service_content
  remote = rclone_remote['crypt']['name']
  cmd = "#{rclone_cmd} copy #{extract_dir} #{remote}:"
  <<-CONTENT.gsub(/^ */, '')
    [Unit]
    Description = #{service_name['export']['crypt']}.service
    Before = #{service_name['global']}.service
    After = network.target #{service_name['extract']}.service

    [Service]
    Type = oneshot
    ExecStart = /bin/bash -c '#{cmd}'
    ExecStartPre = /bin/echo 'Starting test export step to #{remote}'
    ExecStartPost = /bin/echo 'Finished test export step to #{remote}'
  CONTENT
end
# rubocop:enable Metrics/AbcSize

def global_service_content
  exports = service_name['export'].map { |_, v| "#{v}.service" }.join(' ')
  cmd = "ls -tp #{extract_dir} | grep -v \\\'/$\\\' | tail -n +2 " \
      "| xargs -I {} rm -v -- #{extract_dir}/{}"
  <<-CONTENT.gsub(/^ */, '')
    [Unit]
    Description = #{service_name['global']}.service
    After = network.target #{service_name['extract']}.service #{exports}
    Requires = #{service_name['extract']}.service #{exports}

    [Service]
    Type = oneshot
    ExecStart = /bin/bash -c '#{cmd}'
    ExecStartPre = /bin/echo 'Starting test global step (retention: 1)'
    ExecStartPost = /bin/echo 'Finished test global step (retention: 1)'
  CONTENT
end

def global_timer_content
  <<-CONTENT.gsub(/^ */, '')
      [Unit]
      Description = #{service_name['global']}.timer

      [Timer]
      OnCalendar = *-*-* *:*:00

      [Install]
      WantedBy = timers.target
  CONTENT
end

def rclone_remote
  {
    'plain' => {
      'name' => 'local-plain',
      'dir' => '/mnt/backup/plain'
    },
    'crypt' => {
      'name' => 'remote-crypt',
      'dir' => '/mnt/backup/crypt'
    }
  }
end

def rclone_config
  <<-RCLONE_CONF.gsub(/^ */, '')
    [#{rclone_remote['plain']['name']}]
    type = local
    nounc = true

    [local-crypt]
    type = local
    nounc = true

    [#{rclone_remote['crypt']['name']}]
    type = crypt
    remote = local-crypt:#{rclone_remote['crypt']['dir']}
    filename_encryption = standard
    directory_name_encryption = false
    password = RtsjSxAP2klMtZvAh62yZH1dTIdmTFwivQV1-LU-VqRa
    password2 = E1aukiBnxYS_GoAY1lkitFFsddyzBbMDOWDMjSaHUiPbnXNS0cjRUJ8
  RCLONE_CONF
end
