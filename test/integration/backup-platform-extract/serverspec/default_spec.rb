# frozen_string_literal: true

#
# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'
require 'extract_helper'

describe file("/etc/systemd/system/#{service_name}.service") do
  it 'should be a file' do
    expect(subject).to be_file
  end

  it 'should have extract service unit content' do
    expect(subject.content).to eq(service_content)
  end
end

describe command("systemctl status #{service_name}") do
  it 'should be loaded' do
    expect(subject.stdout).to match(/Loaded: loaded/)
  end

  it 'should be inactive' do
    expect(subject.stdout).to match('Active: inactive \(dead\)')
  end
end

describe file(extract_dir) do
  it 'should be a directory' do
    expect(subject).to be_directory
  end

  it 'should be owned by root' do
    expect(subject).to be_owned_by('root')
  end

  it 'should belong to group root' do
    expect(subject).to be_grouped_into('root')
  end

  it 'should be mode 0755' do
    expect(subject).to be_mode(755)
  end
end

describe file("#{extract_dir}/test-#{Time.now.strftime('%F')}") do
  it 'should be a file' do
    expect(subject).to be_file
  end

  it 'should contain test message' do
    expect(subject.content).to eq("test\n")
  end
end
