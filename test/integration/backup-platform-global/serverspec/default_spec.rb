# frozen_string_literal: true

#
# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF serviceKIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'
require 'global_helper'

describe file(rclone_file) do
  it 'should be a file' do
    expect(subject).to be_file
  end

  it 'should have rclone configuration' do
    expect(subject.content).to eq(rclone_config)
  end
end

service_content.each do |service, content|
  describe file("/etc/systemd/system/#{service}.service") do
    it 'should be a file' do
      expect(subject).to be_file
    end

    it "should have #{service.split('-')[2]} service unit content" do
      expect(subject.content).to eq(content)
    end
  end
  describe command("systemctl status #{service}") do
    it 'should be loaded' do
      expect(subject.stdout).to match(/Loaded: loaded/)
    end

    regexp = 'Active: (inactive|activating)'
    it 'should be inactive or activating' do
      expect(subject.stdout).to match(regexp)
    end
  end
end

describe file("/etc/systemd/system/#{service_name['global']}.timer") do
  it 'should be a file' do
    expect(subject).to be_file
  end

  it 'should have global timer unit content' do
    expect(subject.content).to eq(global_timer_content)
  end
end

describe command("systemctl status #{service_name['global']}.timer") do
  it 'should be loaded' do
    expect(subject.stdout).to match(/Loaded: loaded/)
  end

  it 'should be active' do
    expect(subject.stdout).to match('Active: active \(waiting|running\)')
  end
end

sleep(60)

describe file(extract_dir) do
  it 'should be a directory' do
    expect(subject).to be_directory
  end

  it 'should be owned by root' do
    expect(subject).to be_owned_by('root')
  end

  it 'should belong to group wheel' do
    expect(subject).to be_grouped_into('wheel')
  end

  it 'should be mode 0700' do
    expect(subject).to be_mode(700)
  end
end

test_file = "test-#{Time.now.strftime('%F')}"
describe file("#{extract_dir}/#{test_file}") do
  it 'should be a file' do
    expect(subject).to be_file
  end

  it 'should contain test message' do
    expect(subject.content).to eq("test\n")
  end
end

describe file("#{rclone_remote['plain']['dir']}/#{test_file}") do
  it 'should be a file' do
    expect(subject).to be_file
  end
end

crypted_file = `ls #{rclone_remote['crypt']['dir']}`.strip
rclone_remote.each do |type, remote|
  ls_cmd = "#{rclone_cmd} ls #{remote['name']}:"
  cat_cmd = "#{rclone_cmd} cat #{remote['name']}:"

  if type == 'plain'
    file = test_file
    ls_cmd += remote['dir']
    cat_cmd += "#{remote['dir']}/#{test_file}"
  else
    file = crypted_file
    cat_cmd += test_file
  end

  describe file("#{remote['dir']}/#{file}") do
    it 'should be a file' do
      expect(subject).to be_file
    end
  end

  describe command(ls_cmd) do
    it "should list transferred file to #{remote['name']}" do
      expect(subject.stdout).to match("5 #{test_file}")
    end
  end

  describe command(cat_cmd) do
    it "should show content of #{type} file" do
      expect(subject.stdout).to eq("test\n")
    end
  end
end
