# frozen_string_literal: true

#
# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

package 'openssh-server'

service 'sshd' do
  action :start
end

user 'test' do
  home '/home/test'
  shell '/bin/bash'
  manage_home true
  password '$1$U/mhenTE$UDuMimdzdvqwPVxH3V9T61'
end

dir = '/tmp/backup/test'
directory dir do
  recursive true
  action :create
end

file "#{dir}/file-uncrypted" do
  content "this is a test\n"
  action :create
end

directory '/mnt/backup/test' do
  recursive true
  mode '0777'
  action :create
end

service 'backup-test-export-remote-crypt' do
  action :start
end
