Backup platform Cookbook
=============

Description
-----------

This cookbook helps to set up backup cycle using rclone.

A full backup cycle is composed of 3 successives steps each handled by a
systemd service :

- extract command to locally generate backup
- export backup locally or remotely with rclone (encrypted if needed)
- retention command to clear generated backup

Execution order and dependencies are automatically set up depending on
desired backup steps.

Systemd timer to launch backup cycle is set up in retention step
considered as global step.

Backup cycle stops at first failed step in execution order.


Requirements
------------

### Cookbooks and gems

Declared in [metadata.rb](metadata.rb) and in [Gemfile](Gemfile).

### Platforms

A *systemd* managed distribution, tested on :

- RHEL Family 7

Usage
-----

### Test

This cookbook is fully tested through a set up of full backup cycle with
encryption.

This uses kitchen, docker and some monkey-patching.

For more information, see [.kitchen.yml](.kitchen.yml) and [test](test)
directory.

Attributes
----------

Configuration is done by overriding default attributes. All configuration keys
have a default defined in [attributes/default.rb](attributes/default.rb).

Please read it to have a comprehensive view of what and how you can configure
this cookbook behavior.


You can also look at the role written for the tests to have an example on how
to configure this cookbook:
[global](test/integration/roles/backup_platform_global.rb),
[extract](test/integration/roles/backup_platform_extract.rb),
[export](test/integration/roles/backup_platform_export.rb).

Recipes
-------

### default

Include all others recipes.

### install

Install rclone binary

### extract

Enable extract service (backup command).

### export

Enable export service (rclone export command)

### global

Enable global service (timer and retention command).

Resources/Providers
-------------------

None.

Changelog
---------

Available in [CHANGELOG.md](CHANGELOG.md).

Contributing
------------

Please read carefully [CONTRIBUTING.md](CONTRIBUTING.md) before making a merge
request.

License and Author
------------------

- Author:: Vincent Baret (<vbaret@gmail.com>)
- Author:: Sylvain Arrambourg (<saye@sknss.net>)

```text
Copyright (c) 2017-2018 Make.org

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
